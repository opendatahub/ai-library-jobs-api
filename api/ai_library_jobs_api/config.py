import os


class Config(object):
    SECRET_KEY = os.getenv('SECRET_KEY', 'ai-library-jobs-api-secret-key')
    CEPH_S3_ACCESS_KEY = os.environ.get('CEPH_S3_ACCESS_KEY')
    CEPH_S3_SECRET_KEY = os.environ.get('CEPH_S3_SECRET_KEY')
    CEPH_S3_ENDPOINT = os.environ.get('CEPH_S3_ENDPOINT')
    CEPH_S3_BUCKET = os.environ.get('CEPH_S3_BUCKET')

