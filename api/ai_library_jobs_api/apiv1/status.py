import json
import time
from flask import jsonify
from ai_library_jobs_api.apiv1 import apiv1
from ai_library_jobs_api.apiv1 import storage
from ai_library_jobs_api.storage.exceptions import NotFoundError


@apiv1.route('/status', methods=['GET'])
def status():
    status_obj = {
        'version': '0.1',
        'status': 'ok',
        'time': time.strftime('%A %B, %d %Y %H:%M:%S'),
        'bucket': None,
        'date': None
    }

    return jsonify(status_obj)
