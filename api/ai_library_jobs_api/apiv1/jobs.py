import os
import json
import string
import random
from kubernetes import client, config
from pprint import pprint


from flask import jsonify, request, abort
from ai_library_jobs_api.apiv1.errors import *
from ai_library_jobs_api.apiv1 import storage
from ai_library_jobs_api.storage.exceptions import NotFoundError


def rand_str(size=8, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def run_job(name, image, resources, params):
    # for local dev environments when the a kubeconfig is present
    off_cluster = os.getenv('OFF_CLUSTER') in ['True', 'true', '1', 't', 'y', 'yes']
    if off_cluster:
        config.load_kube_config()
        current_namespace = config.list_kube_config_contexts()[1]['context']['namespace']
    else:
        config.load_incluster_config()
        current_namespace = open('/var/run/secrets/kubernetes.io/serviceaccount/namespace').read()

    job_name = name or f'ai-library-job-{rand_str()}'

    job_cpu_request = os.getenv('JOB_CPU_REQUEST', '1000m')
    job_memory_request = os.getenv('JOB_MEMORY_REQUEST', '1Gi')
    job_cpu_limit = os.getenv('JOB_CPU_LIMIT', '1000m')
    job_memory_limit = os.getenv('JOB_MEMORY_LIMIT', '1Gi')
    default_resources = {
        'requests': {
            'cpu': job_cpu_request,
            'memory': job_memory_request
        },
        'limits': {
            'cpu': job_cpu_limit,
            'memory': job_memory_limit
        }
    }

    job_params = params or {}

    job = {
        'apiVersion': 'batch/v1',
        'kind': 'Job',
        'metadata': {
            'name': job_name
        },
        'spec': {
            'parallelism': 1,
            'completions': 1,
            'backoffLimit': 0,
            'template': {
                'metadata': {
                    'name': job_name
                },
                'spec': {
                    'containers': [
                        {
                            'name': job_name,
                            'image': image,
                            'imagePullPolicy': 'Always',
                            'env': [
                                {
                                    'name': 'PARAMS',
                                    'value': json.dumps(job_params)
                                }
                            ],
                            'envFrom': [
                                {
                                    'secretRef': {
                                        'name': 'ai-library-jobs-api'
                                    }
                                }
                            ],
                            'resources': resources or default_resources
                        }
                    ],
                    'restartPolicy': 'Never'
                }
            }
        }
    }

    pprint(job)
    batch_api = client.BatchV1Api()
    api_response = batch_api.create_namespaced_job(current_namespace, job)
    return api_response


@apiv1.route('/jobs/<id>', methods=['GET'])
def get_job(id):
    return jsonify({'id': id})


@apiv1.route('/jobs', methods=['POST'])
def create_job():
    body = json.loads(request.data)
    name = body.get("name")
    image = body["image"]
    resources = body.get("resources")
    params = body.get("params")
    job = run_job(name, image, resources, params)
    return jsonify(job.metadata.name)
