from flask import Blueprint
from ai_library_jobs_api.storage import CephStore
import os

storage = CephStore(
    os.environ.get('S3_PREFIX'),
    host=os.environ.get('S3_ENDPOINT'),
    key_id=os.environ.get('S3_ACCESS_KEY_ID'),
    secret_key=os.environ.get('S3_SECRET_ACCESS_KEY'),
    bucket=os.environ.get('S3_BUCKET'),
    region=None
)

storage.connect()

apiv1 = Blueprint('apiv1', __name__)

from ai_library_jobs_api.apiv1 import errors, status, jobs

