import os

from flask import Flask
from ai_library_jobs_api.config import Config
from ai_library_jobs_api.storage import CephStore


def create_app(app_name):
    ai_library_jobs_api = Flask(app_name, static_folder='ui/build')

    ai_library_jobs_api.config.from_object(Config)

    from ai_library_jobs_api.apiv1 import apiv1
    ai_library_jobs_api.register_blueprint(apiv1, url_prefix='/api/v1')

    return ai_library_jobs_api
