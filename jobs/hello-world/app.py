import os
import boto3
import json
from pprint import pprint


def s3_connect(access_key, secret_key, endpoint):
    try:
        session = boto3.Session(
            aws_access_key_id=access_key, aws_secret_access_key=secret_key)
        s3 = session.resource('s3', endpoint_url=endpoint, verify=False)
    except Exception as ex:
        raise Exception('Error while creating s3 session: ' + str(ex))
    return s3


def main():
    s3_access_key = os.environ.get('S3_ACCESS_KEY_ID')
    s3_secret = os.environ.get('S3_SECRET_ACCESS_KEY')
    s3_endpoint = os.environ.get('S3_ENDPOINT')
    s3_bucket = os.environ.get('S3_BUCKET')
    s3_prefix = os.environ.get('S3_PREFIX')

    # pprint(s3_access_key)
    # pprint(s3_secret)
    # pprint(s3_endpoint)
    # pprint(s3_bucket)
    # pprint(s3_prefix)

    params = os.environ.get('PARAMS')
    param_dict = json.loads(params)
    pprint(param_dict)

    json_file_path = os.path.join(s3_bucket, s3_prefix, 'params.json')
    print(f'Updating JSON file: {json_file_path}')

    s3 = s3_connect(s3_access_key, s3_secret, s3_endpoint)

    obj_contents = s3.Object(s3_bucket, os.path.join(s3_prefix, 'params.json'))
    obj_contents.put(Body=params.encode())

    print(f'{json_file_path} written')
    quit(0)


if __name__ == '__main__':
    main()
