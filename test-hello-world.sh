#!/bin/bash

set -x

API_ROUTE=$(oc describe route ai-library-jobs-api | grep 'Requested Host:' | sed -nr 's/Requested Host:\s*(.*)/\1/p')
URL="$API_ROUTE/api/v1/jobs"

curl "$URL" \
  -X POST \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json' \
  -d '{"image": "hello-world"}'


