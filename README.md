# ai-library-jobs-api


Create a `secrets` file from `secrets.example`
```bash
SECRET_KEY='ChangeMeToAGeneratedSecretKey'
S3_ENDPOINT='http://host:port'
S3_BUCKET='BUCKET-NAME'
S3_PREFIX='dir/subdir'
S3_ACCESS_KEY_ID='ChangeMe'
S3_SECRET_ACCESS_KEY='ChangeMe'
```

Then create the app using the included script.

```bash
./openshift-deploy.sh
```

you can create a hello world job with the included script
```bash
./test-hello-world.sh
```

