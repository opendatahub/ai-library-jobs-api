#!/bin/bash

rm -rf ./venv
virtualenv -p python3 ./venv
source venv/bin/activate

pip install -r api/requirements.txt
pip install -r jobs/hello-world/requirements.txt
